#!/bin/bash
# ------------------------------------------------------------------
# [Author] Kanchana A
#          Pylint test rating
# ------------------------------------------------------------------

VERSION=0.1.0
mkdir -p Output_$(date "+%Y-%m-%d")
# git whatchanged --name-only --pretty="" origin Master 
for i in $(find -name '*.py'); do
echo $i
echo "File Name : ${i##*/}"  
echo "Path : $(realpath $i)";
echo "Rating :-"
python -m pylint --output-format=parseable $i > $(pwd)/Output_$(date "+%Y-%m-%d")/test_output_bash_"${i##*/}".txt;
grep -nr 'rated' $(pwd)/Output_$(date "+%Y-%m-%d")/test_output_bash_"${i##*/}".txt
echo "$(printf %"$(tput cols)"s |tr " " "_")";
done > $(pwd)/Output_$(date "+%Y-%m-%d")/Final_Results.txt
