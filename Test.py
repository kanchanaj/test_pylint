import pylint
from pylint import epylint as ep
from pylint.reporters.text import TextReporter
from io import StringIO
from pylint.lint import Run

import os
directory=os.getcwd()
for root, dirs, files in os.walk(directory):
    for file in files:
        if file.endswith('.py'):
            var = os.path.join(root,file)
            pylint_output = StringIO()  # Custom open stream
            reporter = TextReporter(pylint_output)
            Run('k1.py', reporter=reporter, do_exit=False)
            print(pylint_output.getvalue())  # Retrieve and print the text report

            # (pylint_stdout, pylint_stderr)=ep.py_run('var --disable C0114', return_std=True)
            # print(pylint_stderr)


