from pylint import epylint as ep
import os

directory = os. getcwd()

# iterate over files in
# that directory
for root, dirs, files in os.walk(directory):
    for file in files:
        if file.endswith(".py"):
             l = ep.lint(os.path.join(root, file))
             print(l)







